<?php 
	date_default_timezone_set('Asia/Jakarta'); #untuk wilayah Asia/jakarta (WIB) gunakan set time ini
	$t = date("H:i:s"); #H  HH:MM:SS Hour(H)=jam Minute(i)=menit Seconds(s)=Detik

	#if statement
	// if ($t >= "10"){
	// 	echo "selamat sore, selamat malam";
	// }

	#if.. else statement
	if ($t >= "20"){
		echo "Selamat Siang, selamat sore, selamat malam";
	}else{
		echo "Selamat Pagi";
	};
 ?>